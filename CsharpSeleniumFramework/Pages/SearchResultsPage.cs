﻿using System.Collections.Generic;
using CsharpSeleniumFramework.Utils.Extensions;
using OpenQA.Selenium;

namespace CsharpSeleniumFramework.Pages
{
    public class SearchResultsPage : BasePage
    {
        public IList<IWebElement> SearchResults => Driver.FindElements(By.XPath("//div[contains(@class, 'g')]//div//div//div//h3//a"));

        public RedditPage SelectFirstListedSearchResult()
        {
            SearchResults[0].WeClick();
            return InstanceOf<RedditPage>();
        }
    }
}
