﻿using System;
using CsharpSeleniumFramework.Utils.Extensions;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using static CsharpSeleniumFramework.Utils.Selenium.Settings;

namespace CsharpSeleniumFramework.Pages
{
    public class BasePage : Page
    {
        public IWebDriver Driver { get; internal set; }
        public string GetTitle => Driver.Title;
        public string GetUrl => Driver.Url;
        public string GetPageSource => Driver.PageSource;

        public IWebElement SearchField => Driver.FindElement(By.Name("q"));

        public SearchResultsPage SearchFor(String searchTerm)
        {
            SearchField.WeSendKeys(searchTerm + Keys.Return);
            return InstanceOf<SearchResultsPage>();
        }

        public void NavigateBaseUrl()
        {
            Driver.Navigate().GoToUrl(baseUrl);
            Driver.Manage().Window.Maximize();
            Console.WriteLine("Welcome to the site!");
        }

        public void ValidatePageTitle(string expectedTitle)
        {
            var titleToValidate = GetTitle.Contains(expectedTitle);
            Assert.IsTrue(titleToValidate, ":: This is not the expected title");
            Console.WriteLine(":: The title of the site is" + GetTitle);
        }

        public void ValidatePageUrl(string expectedUrl)
        {
            var urlToValidate = GetUrl.Contains(expectedUrl);
            Assert.IsTrue(urlToValidate, ":: This is not the expected url");
            Console.WriteLine(":: The url is " + GetUrl);
        }

        public void ValidatePageSource(string expectedText)
        {
            var textToValidate = GetPageSource.Contains(expectedText);
            Assert.IsTrue(textToValidate, textToValidate + " is not in the PageSource!");
            Console.WriteLine(":: The text {0} is in the PageSource ", expectedText);
        }

        public void ValidateMultipleInPageSource(Table table)
        {
            foreach (var row in table.Rows)
            {
                var textToValidate = row["expectedText"];

                Assert.IsTrue(GetPageSource.Contains(textToValidate), textToValidate + " is not in the PageSource!");
                Console.WriteLine(":: The text {0} is in the PageSource ", textToValidate);
            }
        }
    }
}
