﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpSeleniumFramework.Utils.Selenium
{
    public class Settings
    {
        public static String baseUrl = "https://www.google.com";
        public static String WeHighlightedColour = "arguments[0].style.border='5px solid green'";
        public static String WdHighlightedColour = "arguments[0].style.border='5px solid orange'";
    }
}
