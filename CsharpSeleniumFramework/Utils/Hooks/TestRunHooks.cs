﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsharpSeleniumFramework.Utils.Selenium;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Utils.Hooks
{
    internal static class TestRunHooks
    {
        [AfterTestRun]
        internal static void AfterTestRun()
        {
            if (!ScenarioContext.Current.ScenarioInfo.Tags.Contains("Debug"))
                DriverController.Instance.StopWebDriver();
        }
    }
}
