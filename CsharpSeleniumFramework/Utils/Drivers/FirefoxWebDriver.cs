﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace CsharpSeleniumFramework.Utils.Drivers
{
    internal static class FirefoxWebDriver
    {
        internal static IWebDriver LoadFirefoxDriver(String firefoxArgument)
        {
            var options = new FirefoxOptions();
            options.AddArgument(firefoxArgument);

            var driver = new OpenQA.Selenium.Firefox.FirefoxDriver(options);
            return driver;
        }
    }
}
