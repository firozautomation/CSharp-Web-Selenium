﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using static CsharpSeleniumFramework.Utils.Selenium.Driver;
using static CsharpSeleniumFramework.Utils.Selenium.Settings;

namespace CsharpSeleniumFramework.Utils.Extensions
{
    public static class WebElementExtensions
    {
        public static void WeHighlightElement(this IWebElement element)
        {
            var js = (IJavaScriptExecutor) Browser();
            js.ExecuteScript(WeHighlightedColour, element);
        }

        public static bool WeElementIsEnabled(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(d =>
                {
                    try
                    {
                        element.WeHighlightElement();
                        return element.Enabled;
                    }
                    catch (StaleElementReferenceException)
                    {
                        return false;
                    }
                }
            );
        }

        public static void WeSelectDropdownOptionByIndex(this IWebElement element, string text, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            new SelectElement(element).SelectByText(text);
        }

        public static void WeSelectDropdownOptionByText(this IWebElement element, string text, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            new SelectElement(element).SelectByText(text);
        }

        public static void WeSelectDropdownOptionByValue(this IWebElement element, string value, int sec = 10)
        {
            element.WeElementIsEnabled(sec);
            new SelectElement(element).SelectByValue(value);
        }

        public static string WeGetAttribute(this IWebElement element, string attribute)
        {
            return element.GetAttribute(attribute);
        }

        public static bool WeElementIsDisplayed(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(d =>
                {
                    try
                    {
                        element.WeHighlightElement();
                        return element.Displayed;
                    }
                    catch (NoSuchElementException)
                    {
                        return false;
                    }
                }
            );
        }

        public static void WeSendKeys(this IWebElement element, string text, int sec = 10, bool clearFirst = false)
        {
            element.WeElementIsDisplayed(sec);
            if (clearFirst) element.Click();
            element.SendKeys(text);
        }

        public static void WeElementToBeClickable(this IWebElement element, int sec = 10)
        {
            var wait = new WebDriverWait(Browser(), TimeSpan.FromSeconds(sec));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void WeElementToBeInvisible(this By element, int sec = 10)
        {
            var wait = new WebDriverWait(Browser(), TimeSpan.FromSeconds(sec));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
        }

        public static void WeClick(this IWebElement element, int sec = 10)
        {
            element.WeElementToBeClickable(sec);
            element.WeHighlightElement();
            element.Click();
        }

        public static void WeSwitchTo(this IWebElement iframe, int sec = 10)
        {
            iframe.WeElementToBeClickable(sec);
            iframe.WeHighlightElement();
            Browser().SwitchTo().Frame(iframe);
        }
    }
}
