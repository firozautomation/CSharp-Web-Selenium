﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using static CsharpSeleniumFramework.Utils.Selenium.Settings;
using static CsharpSeleniumFramework.Utils.Selenium.Driver;

namespace CsharpSeleniumFramework.Utils.Extensions
{
    public static class WebDriverExtensions
    {
        public static object WdHighlight(this By locator)
        {
            var myLocator = Browser().FindElement(locator);
            var js = (IJavaScriptExecutor)Browser();
            return js.ExecuteScript(WdHighlightedColour, myLocator);
        }

        public static IWebElement WdFindElement(this By locator, int sec = 10)
        {
            var wait = new WebDriverWait(Browser(), TimeSpan.FromSeconds(sec));
            return wait.Until(drv =>
                {
                    try
                    {
                        locator.WdHighlight();
                        return drv.FindElement(locator);
                    }
                    catch (NoSuchElementException)
                    {
                        return null;
                    }
                }
            );
        }

        public static void WdSendKeys(this By locator, string text, int sec = 10)
        {
            locator.WdFindElement(sec).SendKeys(text);
        }

        public static void WdClickByIndex(this By locator, int index = 0, int sec = 10)
        {
            var myLocator = Browser().FindElements(locator);
            myLocator[index].Click();
        }

        public static void WdClick(this By locator, int sec = 10)
        {
            locator.WdFindElement(sec).Click();
        }
    }
}
