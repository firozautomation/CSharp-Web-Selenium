﻿using CsharpSeleniumFramework.Pages;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Steps
{
    [Binding]
    public sealed class ReturnClickSteps : BaseSteps
    {
        [When(@"I select the first returned result")]
        public void WhenISelectTheFirstReturnedResult()
        {
            InstanceOf<SearchResultsPage>().SelectFirstListedSearchResult();
        }

    }
}
