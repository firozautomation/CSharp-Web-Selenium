﻿using CsharpSeleniumFramework.Pages;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Steps
{
    [Binding]
    public sealed class ValidationSteps : BaseSteps
    {
        [Then(@"I see the page title contains ""(.*)""")]
        public void ThenISeeThePageTitleContains(string expectedTitle)
        {
            InstanceOf<BasePage>().ValidatePageTitle(expectedTitle);
        }

        [Then(@"I see the page Url contains ""(.*)""")]
        public void ThenISeeThePageUrlContains(string expectedUrl)
        {
            InstanceOf<BasePage>().ValidatePageUrl(expectedUrl);
        }

        [Then(@"I see ""(.*)"" in the PageSource")]
        public void ThenISeeInThePageSource(string expectedText)
        {
            InstanceOf<BasePage>().ValidatePageSource(expectedText);
        }

        [Then(@"I see")]
        public void ThenISee(Table table)
        {
            InstanceOf<BasePage>().ValidateMultipleInPageSource(table);
        }
    }
}
