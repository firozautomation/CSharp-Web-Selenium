﻿using CsharpSeleniumFramework.Pages;
using TechTalk.SpecFlow;

namespace CsharpSeleniumFramework.Steps
{
    [Binding]
    public sealed class SendKeysSteps : BaseSteps
    {
        [When(@"I search for ""(.*)""")]
        public void WhenISearchFor(string searchTerm)
        {
            InstanceOf<BasePage>().SearchFor(searchTerm);
        }

    }
}
